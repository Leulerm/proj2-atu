#include "utils.h"
#include <stdio.h>
#include <stdlib.h>



/* Sign extends the given field to a 32-bit integer where field is
 * interpreted an n-bit integer. */ 
int sign_extend_number( unsigned int field, unsigned int n) {
    
    return ((int) field << (32 - n)) >> (32 - n);
}

/* Unpacks the 32-bit machine code instruction given into the correct
 * type within the instruction struct */ 
Instruction parse_instruction(uint32_t instruction_bits) {
    
    Instruction instruction;
    instruction.opcode = (instruction_bits << 25) >> 25;
    instruction.rtype.rd = (instruction_bits << 20) >> 27;
    instruction.rtype.funct3 = (instruction_bits << 17) >> 29;
    instruction.rtype.rs1 = (instruction_bits << 12) >> 27;
    instruction.rtype.rs2 = (instruction_bits << 7) >> 27;
    instruction.rtype.funct7 = (instruction_bits) >> 25;

    instruction.itype.imm = sign_extend_number((instruction_bits) >> 20, 12);

    instruction.ujtype.imm = sign_extend_number((instruction_bits) >> 12, 20);
    
    instruction.stype.imm5 = (instruction_bits << 20) >> 27;
    instruction.stype.imm7 = (instruction_bits) >> 25;

    return instruction;
}

/* Return the number of bytes (from the current PC) to the branch label using the given
 * branch instruction */
int get_branch_offset(Instruction instruction) {
    int ret = (instruction.sbtype.imm5 & 0b11110) >> 1; 
    ret += ((instruction.sbtype.imm5 & 00001) << 10);
    ret += ((instruction.sbtype.imm7 & 0b1000000) << 5);
    ret += ((instruction.sbtype.imm7 & 0b0111111)) << 4;
    return sign_extend_number(2 * ret, 13);

}

/* Returns the number of bytes (from the current PC) to the jump label using the given
 * jump instruction */
int get_jump_offset(Instruction instruction) {
    int ret = (instruction.ujtype.imm & 0b10000000000000000000) << 1; 
    ret += (instruction.ujtype.imm &    0b01111111111000000000) >> 8;
    ret += (instruction.ujtype.imm &    0b00000000000100000000) << 3;
    ret += (instruction.ujtype.imm &    0b00000000000011111111) << 12;
    return sign_extend_number(ret, 21); 
}

int get_store_offset(Instruction instruction) {
    int ret = instruction.stype.imm5;
    ret += instruction.stype.imm7 << 5;
    return sign_extend_number(ret, 12);
}

void handle_invalid_instruction(Instruction instruction) {
    printf("Invalid Instruction: 0x%08x\n", instruction.bits); 
}

void handle_invalid_read(Address address) {
    printf("Bad Read. Address: 0x%08x\n", address);
    exit(-1);
}

void handle_invalid_write(Address address) {
    printf("Bad Write. Address: 0x%08x\n", address);
    exit(-1);
}
//main() {printf("%d\n", sign_extend_number(0xfff00000 >> 20, 12));}

